*************************************************
Model: Gaussian_pk_Lk_Ck
Transformation: arcsin
*************************************************
Clusters fit: 2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25
Clusters with errors: ---
Selected number of clusters via ICL: 15
ICL of selected model: -692150.9
*************************************************
Number of clusters = 15
ICL = -692150.9
*************************************************
Cluster sizes:
 Cluster 1  Cluster 2  Cluster 3  Cluster 4  Cluster 5  Cluster 6  Cluster 7  Cluster 8  Cluster 9 Cluster 10 Cluster 11 
       425        228        349        306        568        238        321        498        932        393        825 
Cluster 12 Cluster 13 Cluster 14 Cluster 15 
       529        766       1043       1340 

Number of observations with MAP > 0.90 (% of total):
8155 (93.08%)

Number of observations with MAP > 0.90 per cluster (% of total per cluster):
 Cluster 1 Cluster 2 Cluster 3 Cluster 4 Cluster 5 Cluster 6 Cluster 7 Cluster 8 Cluster 9 Cluster 10 Cluster 11 Cluster 12
 405       223       326       268       535       228       294       458       879       367        792        447       
 (95.29%)  (97.81%)  (93.41%)  (87.58%)  (94.19%)  (95.8%)   (91.59%)  (91.97%)  (94.31%)  (93.38%)   (96%)      (84.5%)   
 Cluster 13 Cluster 14 Cluster 15
 698        952        1283      
 (91.12%)   (91.28%)   (95.75%)  

