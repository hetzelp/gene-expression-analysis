import glob
import csv
import pandas as pd
import re

def extract_integers(s):
    return [int(x) for x in re.findall(r'\d+', s)]


#- filepath definitions
query_clusters = glob.glob('3_icis/results/*.tsv')
query_deg_genes = glob.glob('1_coseq/sigGenes_ids/*.txt')
query_exp_genes = glob.glob('1_coseq/sigGenes_ids/downregulated/*.txt')


#- import all icis-target results
query_genes = {} 
#@                     { gene : {
#@                          'cluster' : { cluster_id : NES, ...},
#@                          'exp' : boolean,
#@                          'dexp' : [time_point],
#@                           }
#@                 }

for file in query_clusters:
    try:
        input_str = file.replace('\\', '\\\\')
        index = input_str.find('_', file.find('cluster_') + len('cluster_'))
        cluster_id = int(input_str[input_str.find('cluster_') + len('cluster_'):index+3])
    except ValueError:
        try:
            cluster_id = int(input_str[input_str.find('cluster_') + len('cluster_'):index+2])
        except ValueError:
            cluster_id = file
            print('Error: input string does not contain the cluster_ substring')

    with open(file, 'r') as f:
        reader = csv.reader(f, delimiter="\t")
        header = next(reader)
        for row in reader:
            # @param row : [gene, NES]
            tmp_genes = row[0].split(',')
            for gen in tmp_genes:
                if gen in query_genes.keys():
                    if float(query_genes[gen]['cluster'].get(cluster_id, float('inf'))) < float(row[1]):
                        query_genes[gen]['cluster'][cluster_id] = row[1]
                    else: query_genes[gen]['cluster'][cluster_id] = row[1]
                else:
                    query_genes[gen] = { 
                            'cluster' : {
                                cluster_id : row[1]
                            }
                        }


#! Differentially expressed genes
#- import all differentially expressed genes
deg_genes = {}     #@  {'time_point' : [genes, differantially, expressed]}

for file_path in query_deg_genes:
    time_point = str(extract_integers(file_path)[1]) + 'h'
    lines_list = []
    
    with open(file_path, 'r') as file:
        lines = file.readlines()
        for line in lines:
            line = line.strip()
            if line in deg_genes.keys():
                deg_genes[line].append(time_point)
            else: deg_genes[line] = [time_point]
            
#- add marker if query gene is contained in differentially expressed genes
for gene in query_genes:
    if gene in deg_genes.keys():
        query_genes[gene]['dexp'] = deg_genes[gene]
        

#! Gene expression at 96h and 120h     
#- import all expressed genes at 96h and 120h
exp_genes = {}     #@  {'time_point' : [genes, expressed]}

for file_path in query_exp_genes:
     time_point = str(extract_integers(file_path)[1]) + 'h'
     lines_list = []
    
     with open(file_path, 'r') as file:
          lines = file.readlines()
          for line in lines:
               line = line.strip()
               if line in exp_genes.keys():
                    exp_genes[line].append(time_point)
               else: exp_genes[line] = [time_point]
               
for gene in query_genes:
     if gene in exp_genes.keys():
          query_genes[gene]['exp'] = ['72h']+exp_genes[gene]
     else: query_genes[gene]['exp'] = ['72h']
          
#print(deg_genes)
#print(query_genes)

#! build excel
header = [["Cluster","Enriched_Motif","NES","exp_1", "exp_2", "exp_3", "diff_exp_1", "diff_exp_2", "diff_exp_3"]]
lines = []
for gene in query_genes:
     for cluster in query_genes[gene]['cluster']:
          #? Add      "Cluster","Enriched_Motif","NES"
          tmp_line = [cluster, gene, query_genes[gene]['cluster'][cluster]]
          if 'exp' in query_genes[gene]:
               tmp_line += query_genes[gene]['exp']
               tmp_line += [None] * (3-len(query_genes[gene]['exp']))
          else: tmp_line += [None] * 3
          if 'dexp' in query_genes[gene]:
               tmp_line += query_genes[gene]['dexp']
               tmp_line += [None] * (3-len(query_genes[gene]['dexp']))
          else: tmp_line += [None] * 3
          lines.append(tmp_line)

df = pd.DataFrame(lines, columns = header)

with pd.ExcelWriter('S2-Table_clusters_go_icis.xlsx') as writer:
    df.to_excel(writer, sheet_name= 'Enriched_TFs_clusters_Summary')
    
#print(df)