import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

file_path = "DroID_network.csv"
output_file = "DroID_network_stats.csv"
output_file_top10 = "DroID_network_stats_top10.csv"


# Read the input file and create a pandas DataFrame
df = pd.read_csv(file_path, header=None, names=["Line"])

# Count the number of occurrences of each line
df["Count"] = df["Line"].map(df["Line"].value_counts())
df = df.sort_values(by = ['Count'], ascending=False).drop_duplicates().reset_index(drop = True)

ax = sns.barplot(df, x=df.index, y='Count', dodge=True)
ax = ax.set_xticks(np.arange(0, df.size/2, 100))
plt.xticks(rotation=90)
plt.xlabel("Unique Source Nodes")
plt.ylabel("Number of outgoing connections")

plt.savefig("FIG_S5_1.png", dpi = 300)

df = df[:int(df.size/20)]
ax = sns.barplot(df, x= df.index , y='Count', dodge=True)
ax = ax.set_xticks(np.arange(0, df.size/2, 10))
plt.xticks(rotation=90)
plt.xlabel("")
plt.ylabel("")

plt.savefig("FIG_S5_2.png", dpi = 300)

# ax = df.plot.bar(x='Line', y='Count', rot=0)
# fig = ax.get_figure()
# fig.savefig("output.png")

# Write the DataFrame to a CSV file
df.to_csv(output_file_top10, index=False)