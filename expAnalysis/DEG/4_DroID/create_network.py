import pandas as pd

df1 = pd.read_csv('tf_gene.txt', sep='\t', encoding='ISO-8859-1', header=None)
df2 = pd.read_csv('droid_genetic_interactions.txt', sep='\t', encoding='ISO-8859-1', header=None)

df1_cols = list(df1.columns[:2])
df2_cols = list(df2.columns[:2])

result = pd.merge(df1.iloc[:, :2], df2.iloc[:, :2], how='outer', on=[0,1])
                  
print(result)
result.to_csv('DroID_network_interactions.txt', sep='\t', header=False, index=False)