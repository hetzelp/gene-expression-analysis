import csv
from collections import defaultdict
import numpy as np

input_file = 'countData_72h_96h_120h.tsv'

with open(input_file, 'r') as file:
    csvreader = csv.reader(file, delimiter='\t')
    query_header = next(csvreader)  # get the header row
    query_rows = []
    for row in csvreader:
        query_rows.append(row)

with open('fbgn_fbtr_fbpp_fb_2023_06.tsv', 'r') as file:
    csvreader = csv.reader(file, delimiter='\t')
    gn_header = next(csvreader)  # get the header row
    tr_gn = {}
    for row in csvreader:
        tr_gn[row[0]] = row[1]

FBgn = {}

for row in query_rows:
    row[0] = tr_gn.get(row[0])  # replace FBtr with respective FBgn
    if row[0] in FBgn.keys():
        FBgn_array = FBgn.get(row[0])
        FBgn[row[0]] = [int(x) + int(y) for x, y in zip(FBgn_array, row[1:])]
    else:
        FBgn[row[0]] = row[1:]

print(len(FBgn))

with open('FBgn'+input_file, 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter='\t')
    writer.writerow(query_header)


    # Write the data rows

    for key, value in FBgn.items():

        writer.writerow([key] + value)
