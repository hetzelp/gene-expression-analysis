# RNA-seq

__reference transcript__
FlyBase D. Melanogaster, all transcripts, Release 6.54 (2023-09-21)
http://ftp.flybase.net/releases/current/dmel_r6.54/fasta/dmel-all-transcript-r6.54.fasta.gz

__Experimental Data__
Georg-August-University Goettingen
BioProjects: PRJNA666524, PRJNA374838
Assay Type: RNA-seq
Organisms: @ development stages 72h, 96h, 120h AEL
[D. melanogaster](https://www.ncbi.nlm.nih.gov/Traces/study/?acc=SRP099833&o=acc_s%3Aa)
[D. mauritiana 72h and 96h](https://www.ncbi.nlm.nih.gov/Traces/study/?acc=SRP285860&o=assay_type_s%3Ad&s=SRR12744916,SRR12744917,SRR12744918,SRR12744926,SRR12744937,SRR12744938)
[D. mauritiana 120h](https://trace.ncbi.nlm.nih.gov/Traces/study/?acc=SRP067685&o=acc_s%3Aa&s=SRR3045226,SRR3045225,SRR3045227)
**CAVE: exp. Data for D. mauritiana @ 120h AEL is not provided!** (and nowhere to be found...)

