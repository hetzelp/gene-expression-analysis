# Gene expression analysis

## RNA-seq

1. Download Data
    - `./download_rna.bash`; change Accessions in .bash (e.g. "SRR000...")

### Mapping

1. map reads against strain-specific transcriptomes (including CDS and UTR)
    *Bowtie2-build; build index for FlyBase transcript*
    - download reference genome (*_rna.fna)
    - build bowtie index: `bowtie2-build --threads 20 <reference_in> <bt2_base>`

    *Bowtie2; params: "-very-sensitive-local -N1" [in: *fasta; out: *.sam]*
    - `./align_RNA-seq.bash`; aligns folder and it's subfolders

2. further (?) process reads + count the reads mapped to each transcript
    *idxstats [in: *.sam; out: *.sorted.bam & *.sorted.bam.txt]*
    - `./sam_to_bam.bash`; convert *.sam to .bam, works in folder and it's subfolders
    - `./index_bam.bash`

### Differential expression analysis, Clustering and Go Term Analysis
*build matrix*
    - `python build_matrix.py --path <path to folder + subfolders> run SRR-file1 SRR-file2 ...`

1. pairwise differential expression analysis (`RNA-seq.r`)
    *Rstudio*
    - DeSeq2: apeglm option as schrinkage estimator
        - 72h: melanogaster vs. mauritiana
        - 96h: melanogaster vs. mauritiana
        - 120h: melanogaster vs. mauritiana
    - resLFC <- lfcShrink(dds, coef = "Organism_B.D..mauritiana_vs_A.D..melanogaster", type = "apeglm")

2. clustering - coseq (`RNA-seq.r`)
coseq package (Godichon-Baggioni); params: K=2:25 transformation="arcsin", norm="TMM", model="Normal"
  - export clusters()
  - format cluster.csv with `coseq_go.py`


3. Go Term Analysis - Metascape (Docker Batch Analysis)
    - normal setting
    - source_tax_id and target_tax_id 7227 (D. melanogaster)
    1. `bin/up.sh` - start up metascape container
    2. `bin/ms.sh 1 data/deg/deg.job` - start batch analysis

4. i-cis Target
    - Use Annotation from GO-Analysis
    - Minimum fraction of overlap: 0.4, NES: 3.0, ROC threshold for AUC calculation: 0.01.
    - Paper: Region mapping: 5 kb upstream of the transcription start site, 5’UTR regions and 1st introns using
    - with dm6: only "5kb upstream and full transcript"
    - see "supplementary information.md" in icis results