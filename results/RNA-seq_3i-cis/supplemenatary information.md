
# i-cis Target

1. Analysis Type
Full Analysis

2. region coordinates or gene symbols
Annotations from GO Analysis were uses

3. species
Drosophila melanogaster (dm6)
Gene annotation version: FlyBase r6.02

4. Input type
Gene symbols (FlyBase gene symbols)

5. Database Version
Version 6.0 of databases

6. Features to analyze
PWMs (24453)

7. Job Names
Kxx-Cx_dm6-r6.02
K = Number of coseq cluster
C = Cluster Number


## Optional parameters (same as in paper unless specified else)

Region mapping  -   5kb upstream and full transcript (CAVE: Bei dm6 nur diese Option; dm5 nicht möglich weil dm6 Daten)
Min. fraction of overlap    -   0.4
NES -   3.0
Enrichment analysis     -   Within each database separately
ROC -   0.01
Thres for vis   -   5000


### Additional Information
K16-C5 input Gene Symbols are adjusted to r6.02; only 75% (#274) of original cluster size (#364) matched to r6.02,
K16-C6 input Gene Symbols are adjusted to r6.02; only 77% (#185) of original cluster size (#240) matched to r6.02,

K10-C3 input Gene Symbols are adjusted to r6.02; only 79% (#243) of original cluster size (#305) matched to r6.02,