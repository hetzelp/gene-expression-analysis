# Gene Expression Analysis

## 05.11.23

(RNA-seq)
Tagesziel: DeSeq2 - pairwise differential expression analysis step zumindest einnmal erfolgraich ausführen

- Aufgefallenes Problem: für bowtie step wurden die jeweiligen references genutzt und nicht eins für alle => build_matrix.py funktioniert nur für gleiche id's, deshalb keine Verarbeitung möglich
- Lösungsansatz: align_RNA-seq.bash mit reference von D. melanogaster auch für D. mauritiana benutzten
- aligning SRR12744926 fasta...
    274907375 reads; of these:these:
        274907375 (100.00%) were unpaired; of these:
            14597209 (5.31%) aligned 0 times
            225660829 (82.09%) aligned exactly 1 time
            34649337 (12.60%) aligned >1 times
        94.69% overall alignment rate
    => es funktioniert ganz gut auch mit ref-genom von anderem Organismus
- alignment braucht jetzt erheblich länger als zuvor, nichtsdestotrotz bis zu 95% overall alignment rate

End of Day: Tagesziel nicht erreicht; Erkenntnis: read mapping (bowtie) muss mit D. melanogaster Referenz durchgeführt werden

## 06.11.23

(RNA-seq)
Tagesziele:
    (a) alle ausstehenden read mappings generieren,
    (b) alle Matrizen für DESeq2 generieren,
    (c) DeSeq2 - pairwise differential expression analysis step zumindest einnmal erfolgraich ausführen

- (a) erfolgreich erledigt
- SRA und Referenzdaten in separaten Ordner sortiert um Speicherplatz frei zu machen (aktuell ca. 1,5TB in Benutzung)
- (b) erfolgreich erledigt; Daten scheinen auch zu passen
- Aufgefallen, dass RNA-seq Daten von 120h AEL fehlen; sind im BioProject auch nicht enthalten (nur ATAC-seq); Korrektur: D. Melanogaster vorhanden
- Bei der Suche nach 120h-Daten ist mir aufgefallen, dass FlyBase transcripts genommen wurden => alles von Anfang

End of Day: bowtie läuft für alle RNA-seq mit transcripts von FlyBase

## 07.11.23

(RNA-seq)
In der Nacht von gestern auf heute wurden alle Daten fertig prozessiert.
Tagesziele: siehe 06.11.23 (b-c)

- (b) erfolgreich erledigt
- (c) erfolgreich erledigt, Resultate ähnlich zu denen aus dem Paper

End of Day: Pairwise Differential Expression Analysis abgeschlossen
Next Up: Downstream tasks - Clustering/coseq, Go Term Analysis/Metascape \ TF binding Site Enrichment/i-cis Target

## 13.11.23

(RNA-seq)
Tagesziel: Clustering machen und Visualisierungen erstellen

- clustering hat geklappt
- Visualisierung ist bisschen ein Problem, zumal die fehlenden Daten von 120h AEL eine Interpreation schwierig machen
- alles ein wenig deprimierend, da ich richtig schwierigkeiten im Umgang mit R habe

End of Day: coseq *check*, cluster visualisierung... ganz weit weg von check

## 14.11.23

(RNA-seq)
Tagesziel: (a) fehlende RNA-seq Daten bei Autoren anfragen (b) mit ATAC-seq weiter machen

- (a) Mail an Nico Posnie gesendet; hat auch geantwortet und die Daten übersendet, verarbeitung vsl. morgen

(ATAC-seq)

- einarbeitung in die de novo motif finding; workflow ist relativ schlecht beschrieben => Standardparameter

End of Day: ATAC-seq Workflow im Bereich der Chip-Chip Daten und Motif finding muss morgen weiter verbessert werden. Aktuell noch keinen guten Ansatz gefunden

## 15.11.23

(ATAC-seq)
- de novo motif search erfolgreich durchgeführt

## 16.11.23 bis 18.11.23

Nach ewigem hin und her war ich wieder soweit zu checken, dass ich nichts gecheckt habe.
FÜr ATAC-seq Daten werden die FBgn daten verwendet und nicht FBtr wie bei RNA-seq
