# align_RNA-seq.bash
#* IN: *.fasta ; OUT: *.sam
#! map reads against transcriptomes
#? (1) Place script in folder with *.fasta files or subfolders containing *.fasta files
#? (2) build bowtie index (-> bowtie-build) and place it into subfolder "reference"
#? (3) change parameters of machine your running
#? (4) have fun running this!

#!/bin/bash
echo Start: `date +"%T"`

#! change those according to you system
available_threads=20           #@param $available_threads : thread count used by bowtie
reference_name=${PWD##*/}       #@param $reference_name : name of reference in folder "reference", default folder name script is located in
#! ---

search_dir="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"  #@param $search_dir : full filepath of folder script is located at
script_dir=$search_dir                                              #@param $script_dir : copy of $search_dir

echo > files_to_align.txt											#? create temporary text file to store filepaths

function list_dir() {                                               #> cicle through items in a folder and its subfolders
    for entry in "$search_dir"/*                                        #? for every item in a folder
        do
            if [[ -d $entry ]]                                              #? if item is a folder
                then
                    search_dir=$entry
                    list_dir "$search_dir $script_dir"                          #? call function 'list_dir'
                else                                                        #? if item is a file => do something
                    if [[ $entry == *.fasta ]]                                  #? if file is a .fasta-file
                        then
							#echo "$script_dir/files_to_align.txt"
							echo "$entry" >> $script_dir/files_to_align.txt         #? add path of file into .txt-file
                    fi
            fi
        done
}

list_dir "$search_dir"												#* call function list_dir


                                        #@ param current_dir: name of folder script is located at
files_to_align=$(cat "files_to_align.txt")                          #@param $files_to_align : location of the temporary text file to store filepaths 

for filepath in $files_to_align; do
filename=${filepath##*/}
filename=${filename%.*}
	if [[ -f "$filename.sam" ]]
		then
			echo "${filepath##*/} was already aligned"
		else
			echo "aligning ${filepath##*/}... "
    		bowtie2 --very-sensitive-local -N 1 --threads $available_threads -f -x "reference/$reference_name" -U "$filepath" -S "$filename.sam"
	fi
done

rm $script_dir/files_to_align.txt									#? delete temporary text file to store filepaths

echo Finish: `date +"%T"`