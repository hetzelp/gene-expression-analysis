#!/bin/sh

simulans_72h="SRR12744913 SRR12744914 SRR12744915"
mauritiana_72h="SRR12744926 SRR12744937 SRR12744938"
mauritiana_96h="SRR12744916 SRR12744917 SRR12744918"
mauritiana_120h="SRR3045225 SRR3045226 SRR3045227"
melanogaster_72h="SRR5259207 SRR5259209 SRR5259210"
melanogaster_96h="SRR5259212 SRR5259213 SRR5259215"
melanogaster_120h="SRR5259216 SRR5259217 SRR5259218"



#! Simulans 72h
for item in $simulans_72h
do
    echo "$item"
    fasterq-dump $item --outdir "/home/phetzel/gene-expression-analysis/seq_data/SRA/simulans/72h" --threads 20 --progress --mem 2048MB --fasta
done


#! mauritiana_72h
for item in $mauritiana_72h
do
    echo "$item"
    fasterq-dump $item --outdir "/home/phetzel/gene-expression-analysis/seq_data/SRA/mauritiana/72h" --threads 20 --progress  --mem 2048MB --fasta
done

#! mauritiana_96h
for item in $mauritiana_96h
do
    echo "$item"
    fasterq-dump $item --outdir "/home/phetzel/gene-expression-analysis/seq_data/SRA/mauritiana/96h" --threads 20 --progress --mem 2048MB --fasta
done

#! mauritiana_120h
for item in $mauritiana_120h
do
    echo "$item"
    fasterq-dump $item --outdir "/home/phetzel/gene-expression-analysis/seq_data/SRA/RNA-seq/mauritiana/120h" --threads 20 --progress --mem 2048MB --fasta
done


#! melanogaster_72h
for item in $melanogaster_72h
do
    echo "$item"
    fasterq-dump $item --outdir "/home/phetzel/gene-expression-analysis/seq_data/SRA/melanogaster/72h" --threads 20 --progress  --mem 2048MB --fasta
done

#! melanogaster_96h
for item in $melanogaster_96h
do
    echo "$item"
    fasterq-dump $item --outdir "/home/phetzel/gene-expression-analysis/seq_data/SRA/melanogaster/96h" --threads 20 --progress  --mem 2048MB --fasta
done

#! melanogaster_120h
for item in $melanogaster_120h
do
    echo "$item"
    fasterq-dump $item --outdir "/home/phetzel/gene-expression-analysis/seq_data/SRA/melanogaster/120h" --threads 20 --progress  --mem 2048MB --fasta
done