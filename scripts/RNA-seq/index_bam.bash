#!/bin/bash
echo Start: `date +"%T"`

search_dir="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"  #@param search_dir: full filepath of folder script is located at

# Use the getopts command to parse the arguments
while getopts 'p:' opt; do
 case $opt in
    p) 
      search_dir="$OPTARG"
      ;;
    \?) 
      echo "Invalid option -$OPTARG" >&2 
      ;;
 esac
done

echo "Searchpath: $search_dir"

function list_dir() {                                               #> cicle through items in a folder and its subfolders
    for entry in "$search_dir"/*                                        #? for every item in a folder
        do
            if [[ -d $entry ]]                                              #? if item is a folder
                then
                    search_dir=$entry
                    list_dir "$search_dir $script_dir"                          #? call function 'list_dir'
                else                                                        #? if item is a file => do something
                    if [[ $entry == *.bam && $entry != *.sorted.bam ]]                                 #? if file is a .bam-file
                        then
                            echo "sorting $entry"
                            samtools sort -@ 20 -m 1G --output-fmt bam --output-fmt-option nthreads=20 -T tempbam -o "${entry%.*}.sorted.bam" $entry

                            echo "indexing ${entry%.*}"
                            samtools index -@ 20 "${entry%.*}.sorted.bam" -o "${entry%.*}.sorted.bam.bai"

                            echo "running idxstats for ${entry%.*}"
                            samtools idxstats -@ 20 "${entry%.*}.sorted.bam" &> "${entry%.*}.sorted.bam.txt"
                    fi
            fi
        done
}

list_dir "$search_dir"
echo Finish: `date +"%T"`
