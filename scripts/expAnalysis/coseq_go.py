import os
import csv
import pandas as pd

# Define the path to your input tsv file
input_file_name = "coseq-clusters_all.csv"

# Define the path to your output tsv file
output_file_name = "coseq-clusters_table_all.csv"
output_file_path = 'cluster'
os.mkdir(output_file_path)

# Define a dictionary to store the group values
group_dict = {}

# Read the input tsv file
with open(input_file_name, 'r') as input_file:
    reader = csv.reader(input_file, delimiter=',')

    # Iterate through each row in the tsv file
    for row in reader:
        # Get the first column value (group value) and second column value (id)
        group_value = row[1]
        id_value = row[0]

        # If the group value is not already in the dictionary, add it
        if group_value not in group_dict:
            group_dict[group_value] = []

        # Append the id value to the corresponding group value in the dictionary
        group_dict[group_value].append(id_value)


max_length = max([len(item) for item in group_dict.values()])

for key, value in group_dict.items():
    if len(value) < max_length:
        group_dict[key] = value + [''] * (max_length - len(value))

clusters = pd.DataFrame(group_dict)
print(clusters)
clusters.to_csv(output_file_name, index=False)

# Iterate through each column in the dataframe
for column in clusters.columns:
    # Create a new dataframe with the current column as the data and the column name as the header
    new_df = pd.DataFrame(clusters[column].values, columns=[column])
    
    # Write the new dataframe to a new csv file with the column name as the filename
    new_df.to_csv(output_file_path + f'/c{column}.txt', index=False, header=None)
