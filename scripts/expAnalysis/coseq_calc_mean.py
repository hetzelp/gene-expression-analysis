import pandas as pd

df = pd.read_csv('K16-profiles.csv', header = 0)

rlt = pd.DataFrame()
rlt["FBtr"] = df.iloc[:, 0:1]

rlt["OreR72h"] = df.iloc[:, 5:7].mean(axis = 1, numeric_only = True)
rlt["OreR96h"] = df.iloc[:, 8:10].mean(axis = 1, numeric_only = True)
rlt["OreR120h"] = df.iloc[:, 2:4].mean(axis = 1, numeric_only = True)

rlt["TAM72h"] = df.iloc[:, 14:16].mean(axis = 1, numeric_only = True)
rlt["TAM96h"] = df.iloc[:, 17:19].mean(axis = 1, numeric_only = True)
rlt["TAM120h"] = df.iloc[:, 11:13].mean(axis = 1, numeric_only = True)

rlt.to_csv("K16-profiles-mean.csv", sep = ',')