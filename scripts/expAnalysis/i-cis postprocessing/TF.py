import csv
import argparse

def read_csv(file_path):
    with open(file_path, 'r') as csvfile:
        lines = []
        reader = csv.reader(csvfile, delimiter='\t')
        for row in reader:
            lines.append(row)
    return lines
    
def filter_Feature_and_Score(lines, columns_to_extract, min_NES):
    cluster = {}
    filtered_lines = []
    
    for line in lines:

        tmp_line = []

        append_line = False
        for column_idx in columns_to_extract:
            tmp_column = line[column_idx]
            
            if tmp_column == '': break
            
            if tmp_column.replace('.', '', 1).replace('-','',1).isdigit():
                if float(tmp_column) < min_NES: break
                else: append_line = True

            tmp_line.append(line[column_idx])


        if append_line:
            filtered_lines.append(tmp_line)
        else: continue

    return(filtered_lines)

def format_values(lines):
    dict = {}
    for line in lines:
        if line[0] in dict:
            if line[1] in dict[line[0]]:
                tmp_dict = dict[line[0]]
                tmp_dict[line[1]].append(line[2])
            else:
                dict[line[0]][line[1]] = [line[2]]
        else:
            tmp_dict={}
            tmp_dict[line[1]] = [line[2]]
            dict[line[0]] = tmp_dict
    print(dict)
    return dict

def write_dict_to_file(dict, basepath, output_file):
    
    for cluster in dict:
        with open(basepath + "/" + cluster + output_file, 'w') as file:
            file.write('Annotation\tNES\n')
            for key in dict[cluster]:
                file.write(key)
                for values in dict[cluster].get(key):
                    file.write('\t'+values)
                file.write('\n')



#> Argument Parser
def parse_arguments():
    # Create the argument parser
    parser = argparse.ArgumentParser()

    # Add an optional argument
    parser.add_argument('-p', '--path', help='path to folder', type = str)
    parser.add_argument('-i', '--input', help='name of input file; *.tbl', type = str, default='statistics.tbl')
    parser.add_argument('-o', '--output', help='name of output file; *.tsv', type = str, default = 'Annotation_wNES.tsv')
    parser.add_argument('-n', '--nes', help='NES threshold', type = int, default=4)

    # Parse the arguments
    args = parser.parse_args()

    return args


#- main method        
def main():
    args = parse_arguments()

    basepath = args.path
    input_file = basepath + "/" + args.input
    output_file = basepath + "/" + args.output

    columns_to_extract = [0,4,7]
    min_NES = args.nes

    csv_file = read_csv(input_file)
    csv_filtered = filter_Feature_and_Score(csv_file, columns_to_extract, min_NES)
    csv_dict = format_values(csv_filtered)
    write_dict_to_file(csv_dict, basepath, args.output)

if __name__ == '__main__':
    main()