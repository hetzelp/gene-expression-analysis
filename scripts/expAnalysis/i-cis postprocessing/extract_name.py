import os

def read_file(file_path):
    with open(file_path, 'r') as file:
        return file.readlines()

def search_matches(file_a, file_b):
    lines_b = read_file(file_b)
    matches = []

    for line_a in file_a:
        line_a = line_a.strip()

        for line_b in lines_b:
            line_b = line_b.strip()

            if line_a in line_b:
                matches.append(f"{line_a}")

    return matches

def write_matches(matches, output_file):
    with open(output_file, 'w') as file:
        lines_seen = set()
        for match in matches:
            if match not in lines_seen:
                file.write(f"{match}\n")
                lines_seen.add(match)


if __name__ == "__main__":
    file_a = read_file('cluster.txt')
    file_b = 'dm6-r6.02.txt'
    matches = search_matches(file_a, file_b)
    write_matches(matches, 'output.txt')