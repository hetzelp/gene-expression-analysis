#! /bin/bash

start=`date +"%T"`

search_dir="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"  #@param search_dir: full filepath of folder script is located at

# Use the getopts command to parse the arguments
while getopts 'p:' opt; do
 case $opt in
    p) 
      search_dir="$OPTARG"
      ;;
    \?) 
      echo "Invalid option -$OPTARG" >&2 
      ;;
 esac
done

function list_dir() {                                               #> cicle through items in a folder and its subfolders
    for entry in "$search_dir"/*                                        #? for every item in a folder
        do
            if [[ -d $entry ]]                                              #? if item is a folder
                then
                    search_dir=$entry
                    list_dir "$search_dir $script_dir"                          #? call function 'list_dir'
                else                                                        #? if item is a file => do something
                    # if [[ $entry == *.centered.bed ]]                                  #? if file is a .bed-file
                    #     then
                    #         echo "centering $entry "
					#         macs2 callpeak -t $entry -g dm --nomodel --nolambda --shift -100 --extsize 200 -q 0.01 --bdg -f BEDPE --keep-dup all --outdir ${entry%%.*} 2> "${entry%%.*}.log"
                    # fi

                    if [[ $entry == *_rmD.bed ]]                                  #? if file is a .bed-file
                        then
                            echo "call peaks on $entry "
					        macs2 callpeak -t $entry -g dm --nomodel --shift -100 --extsize 200 -q 0.01 --bdg --outdir ${entry%%.*} 2> "${entry%%.*}.log"
                    fi
            fi
        done
}

list_dir "$search_dir"

echo Start: $start
echo Finish: `date +"%T"`