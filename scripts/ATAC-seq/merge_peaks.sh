#!/bin/bash

echo Start: `date +"%T"`

search_dir="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"  #@param search_dir: full filepath of folder script is located at

# Use the getopts command to parse the arguments
while getopts 'p:' opt; do
 case $opt in
    p) 
      search_dir="$OPTARG"
      ;;
    \?) 
      echo "Invalid option -$OPTARG" >&2 
      ;;
 esac
done


function list_dir() {                                               #> cicle through items in a folder and its subfolders

    for entry in "$search_dir"/*                                        #? for every item in a folder
        do
            if [[ -d $entry ]]                                              #? if item is a folder
                then
                    search_dir=$entry
                    list_dir "$search_dir $script_dir"                      #? call function 'list_dir'
                else                                                        #? if item is a file => do something
                    if [[ $entry == *.narrowPeak ]]                         #? if file is a narrowPeak file
                        then
                            cut -f 1-6 $entry > "${entry%.*}.bed"           #? convert *.narrowPeak to *.bed file
                        else
                            rm $entry
                    fi
            fi
        done
    
    find $search_dir -type f -name "*.bed" -print0 | xargs --null cat > "$search_dir/NA_peaks_merged.bed"           #? merge all bed-files into one bed file
    sort -k 1,1 -k2,2n "$search_dir/NA_peaks_merged.bed" > "$search_dir/NA_peaks_merged_sorted.bed"                 #? sort bed file by chromosome and start position
    bedtools merge -i "$search_dir/NA_peaks_merged_sorted.bed"  > "$search_dir/merged_peaks_woID.bed"               #? merge peaks
    awk -F'\t' 'BEGIN {OFS=FS} {if ($4 == "") $4 = "peak_" counter++; print}' "$search_dir/merged_peaks_woID.bed" > "$search_dir/merged_peaks.bed"  #? add Unique Peak ID "peak_ID"

    
    for entry in "$search_dir"/*                                        #? for every item in a folder
        do
            if [[ -d $entry ]]                                              #? if item is a folder
                then
                    search_dir=$entry
                    list_dir "$search_dir $script_dir"                      #? call function 'list_dir'
                else                                                        #? if item is a file => do something
                    if [[ $entry == *_peaks.bed ]]                         #? if file is a narrowPeak file
                        then
                            mkdir "$search_dir/peakAnalysis"> /dev/null 2>&1

                            findMotifsGenome.pl $entry \
                            /mnt/Bachelorarbeit/gene-expression-analysis/seq_data/SRA/ATAC-seq/reference/dmel-wo_mito-chromosome-r6.54.fasta \
                            "$search_dir/peakAnalysis/" \
                            -find /mnt/Bachelorarbeit/gene-expression-analysis/seq_data/SRA/Chip-chip/pnr/motif5.motif \
                            -opt /mnt/Bachelorarbeit/gene-expression-analysis/seq_data/SRA/Chip-chip/pnr/motif5.similar1.motif /mnt/Bachelorarbeit/gene-expression-analysis/seq_data/SRA/Chip-chip/pnr/motif5.similar2.motif \
                            -p 20 \
                            -cache 2000 \
                            -preparse \
                            > "$search_dir/peakAnalysis/peakAnalysis.tsv" 2> "$search_dir/peakAnalysis/analysis_log.txt"
                            #-size 50 \
                            cut -f 1 "$search_dir/peakAnalysis/peakAnalysis.tsv" | sort | uniq -c | wc -l
                    fi
            fi
        done
}

list_dir "$search_dir"

echo Finish: `date +"%T"`