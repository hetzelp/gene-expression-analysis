#! /bin/bash

echo Start: `date +"%T"`

search_dir="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"  #@param search_dir: full filepath of folder script is located at

# Use the getopts command to parse the arguments
while getopts 'p:' opt; do
 case $opt in
    p) 
      search_dir="$OPTARG"
      ;;
    \?) 
      echo "Invalid option -$OPTARG" >&2 
      ;;
 esac
done

function list_dir() {                                               #> cicle through items in a folder and its subfolders
    for entry in "$search_dir"/*                                        #? for every item in a folder
        do
            if [[ -d $entry ]]                                              #? if item is a folder
                then
                    search_dir=$entry
                    list_dir "$search_dir $script_dir"                          #? call function 'list_dir'
                else                                                        #? if item is a file => do something
                    if [[ $entry == *annotation.csv ]]                                  #? if file is annotation.csv
                        then
                            awk -F'\t' '$10 != "NA"' $entry > "${entry%.*}_filtered.csv"    #? Filter out all lines with no annotations
                            dir=$(dirname $entry)                                           
                            python filter_peaks.py --path "$dir" --annotation "annotation_filtered.csv" --input "NA_peaks.narrowPeak" --output "NA_peaks_filtered.narrowPeak"                          #? Filter out all lines with no annotations from narrowPeak file
                    fi
            fi
        done
}

list_dir "$search_dir"

echo Finish: `date +"%T"`