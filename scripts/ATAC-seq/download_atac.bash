#!/bin/sh

simulans="SRR12744919 SRR12744920 SRR12744921 SRR12744922 SRR12744923"
mauritiana="SRR12744924 SRR12744925 SRR12744927 SRR12744928 SRR12744929"
melanogaster="SRR12744930 SRR12744931 SRR12744932 SRR12744933 SRR12744934"

#! simulans
for item in $simulans
do
    echo "$item"
    fasterq-dump $item --outdir "/home/phetzel/gene-expression-analysis/seq_data/ATAC-seq/simulans" --threads 20 --progress --mem 2048MB --fasta
done

#! mauritiana
for item in $mauritiana
do
    echo "$item"
    fasterq-dump $item --outdir "/home/phetzel/gene-expression-analysis/seq_data/ATAC-seq/mauritiana" --threads 20 --progress --mem 2048MB --fasta
done


#! melanogaster
for item in $melanogaster
do
    echo "$item"
    fasterq-dump $item --outdir "/home/phetzel/gene-expression-analysis/seq_data/ATAC-seq/melanogaster" --threads 20 --progress  --mem 2048MB --fasta
done