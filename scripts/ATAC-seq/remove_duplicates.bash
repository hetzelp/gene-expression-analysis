echo Start: `date +"%T"`

search_dir="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"  #@param search_dir: full filepath of folder script is located at

# Use the getopts command to parse the arguments
while getopts 'p:' opt; do
 case $opt in
    p) 
      search_dir="$OPTARG"
      ;;
    \?) 
      echo "Invalid option -$OPTARG" >&2 
      ;;
 esac
done

function list_dir() {                                               #> cicle through items in a folder and its subfolders
    for entry in "$search_dir"/*                                        #? for every item in a folder
        do
            if [[ -d $entry ]]                                              #? if item is a folder
                then
                    search_dir=$entry
                    list_dir "$search_dir $script_dir"                          #? call function 'list_dir'
                else                                                        #? if item is a file => do something
                    if [[ $entry == *_sorted.bam ]]                                  #? if file is a .fasta-file
                        then
                            echo "removing duplicates from $entry "
					        picard MarkDuplicates -I $entry -M "${entry%.*}.metrics" -O "${entry%.*}_rmD.bam" --REMOVE_DUPLICATES true
                            bedtools bamtobed -i "${entry%.*}_rmD.bam" > "${entry%.*}_rmD.bed"
                    fi
            fi
        done
}

list_dir "$search_dir"

echo Finish: `date +"%T"`