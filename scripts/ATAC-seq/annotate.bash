#! /bin/bash

start=`date +"%T"`

search_dir="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"  #@param search_dir: full filepath of folder script is located at

# Use the getopts command to parse the arguments
while getopts 'p:' opt; do
 case $opt in
    p) 
      search_dir="$OPTARG"
      ;;
    \?) 
      echo "Invalid option -$OPTARG" >&2 
      ;;
 esac
done

function list_dir() {                                               #> cicle through items in a folder and its subfolders
    for entry in "$search_dir"/*                                        #? for every item in a folder
        do
            if [[ -d $entry ]]                                              #? if item is a folder
                then
                    search_dir=$entry
                    list_dir "$search_dir"                          #? call function 'list_dir'
                else                                                        #? if item is a file => do something
                    if [[ $entry == *_summits.bed ]]                                  #? if file is a .fasta-file
                        then
                            echo "annotating $entry"
					        annotatePeaks.pl $entry dm6 -gtf /mnt/Bachelorarbeit/gene-expression-analysis/seq_data/SRA/ATAC-seq/dmel-all-r6.54.gtf > $search_dir/annotation.csv
                    fi
            fi
        done
}

list_dir "$search_dir"

echo Start: $start
echo Finish: `date +"%T"`