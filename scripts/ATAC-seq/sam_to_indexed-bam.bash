#!/bin/bash
echo Start: `date +"%T"`
# Set the default values for the arguments
search_dir="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"  #@param search_dir: full filepath of folder script is located at

# Use the getopts command to parse the arguments
while getopts 'p:' opt; do
 case $opt in
    p) 
      search_dir="$OPTARG"
      ;;
    \?) 
      echo "Invalid option -$OPTARG" >&2 
      ;;
 esac
done


echo "Searchpath: $search_dir"
multiqc_dir=$search_dir

function list_dir() {                                               #> cicle through items in a folder and its subfolders
    for entry in "$search_dir"/*                                        #? for every item in a folder
        do
            if [[ -d $entry ]]                                              #? if item is a folder
                then
                    search_dir=$entry
                    list_dir "$search_dir $script_dir"                          #? call function 'list_dir'
                else                                                        #? if item is a file => do something
                    if [[ $entry == *.sam ]]                                  #? if file is a .fasta-file
                        then
                            echo "converting $entry "
					        samtools view -@ 20 -h -S -b -o "${entry%.*}.bam" $entry

                            echo "sorting ${entry%.*}"
                            samtools sort "${entry%.*}.bam" --threads 20 -m 1G --output-fmt bam -o "${entry%.*}_sorted.bam"

                            echo "indexing ${entry%.*}"
                            samtools index -@ 20 "${entry%.*}_sorted.bam" -o "${entry%.*}_sorted.bam.bai"
                        # else 
                        #     echo "no *.sam files found in $search_dir"
                    fi
            fi
        done
}

list_dir "$search_dir"

multiqc $multiqc_dir

echo "done"

echo Finish: `date +"%T"`