#!/bin/sh

organism="<Accession>"

#! organism_A                                                                           
# copy this block for downloading multiple accessions ind multiple locations
for item in organism
do
    echo "$item"
    fasterq-dump $item --outdir "<path>" --threads 20 --progress --mem 2048MB --fasta
    #@param --threads:  change thread count of processor minus 2 for the system
    #@param --progress: give progress on stdout
    #@param --mem:      memory to use, default is 1GB
    #@param --fasta:    output as fasta
done
#!