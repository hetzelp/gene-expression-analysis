# Gene expression analysis

## ATAC-seq

1. Download Data
    - `./download_atac.bash`; change Accessions in .bash (e.g. "SRR000...")

### Mapping

1. reads were maooed to D.melanogaster genome
    *Bowtie2-build; build index for FlyBase transcript*
    - download reference genome (*_rna.fna)
    - build bowtie index: `bowtie2-build --threads 20 <reference_in> <bt2_base>`

    *Bowtie2; params: "-no-unal -X2000"
    - `./align_ATAC-seq.bash`; aligns folder and it's subfolders

2. convert sam to bam, sort an index bam, remove duplicates
    *samtools*
    - `./sam_to_indexed-bam.bash`; convert *.sam to .bam, works in folder and it's subfolders

    *Picard; default params, convert resulted bam to bed*
    - `./remove_duplicates.bash` as

### Peak Calling and Annotation

1. Peak Calling
    *macs2; -g dm --nomodel --shift -100 --extsize 200 -q 0.01*
    - `./call_peaks.bash`

2. Annotation
    *HOMER*
    - `./annotate.bash`
    *awk - filter annotations*
    - `./filter_annotation.bash`

3. Find pnr motif in (filtered) peaks
    - copy filtered narrowPeaks file in seperate folder
    - `./merge_peaks.sh`
    - `python filter_peaks.py --path "path/to/folder" --annotation "peakAnalysis/peakAnalysis.tsv" --input "merged_peaks.bed" --output "merged_summits.bed"`
    - `./annotate.bash`
    - `sort -t$'\t' -k12,12 -u annotation.csv > annotation_unique.csv;cut -f 12 annotation_unique.csv > annotation_genes_only.txt`
    - `significant_genes.py`
