# ATAC-seq

Bowtie2:
reference: discarded mitochondiral gene

`bowtie2 --no-unal -X 2000 --threads 20 -f -x "reference/melanogaster" -U "$filepath" -S "$filename.sam" --met-file "$filename.bowtie.metrics"`